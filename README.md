# gitlab-ci-tasks

An opinionated and comprehensive extensible GitLab CI task config

This contains out-of-the-box support for [Semantic Release](https://semantic-release.gitbook.io/semantic-release)
and multi-arch Docker manifests.

# Getting Started

> You may wish to fork this repo. All the containers should build automatically on
> first run. All links in this documentation will be to the original repo.

This repo makes use of the GitLab [include](https://docs.gitlab.com/ee/ci/yaml/#includefile)
functionality, introduced in GitLab 11.7.

## Configure your repo

Generate a [personal access token](https://gitlab.com/profile/personal_access_tokens)
with API access and add it's value to the `GITLAB_TOKEN` key in your CI/CD variables.
By default, the only branch that requires this is your `master` branch, so it is
recommended that you set it as both `protected` and `masked`.

## GitLab Config

Create a `.gitlab-ci.yml` file in the root of your repo.

### Include the tasks

```yaml
include:
  - project: "mrsimonemms/gitlab-ci-tasks"
    ref: "master" # A tagged version is preferable in production
    file: "tasks/common.yml"
```
